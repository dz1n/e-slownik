# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'uwaga.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def __init__(self):
        self.mess = ''

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(400, 165)
        Dialog.setFixedSize(400, 165)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/loga/images/fav.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setStyleSheet("background-color: rgb(55 ,55, 55);\n"
                             "color: rgb(255, 255, 255);")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(0, 110, 101, 51))
        self.label.setStyleSheet(f"border-image: url(:loga/images/Logo1.png);")
        self.label.setText("")
        self.label.setObjectName("label")
        self.label_19 = QtWidgets.QLabel(Dialog)
        self.label_19.setGeometry(QtCore.QRect(110, 110, 51, 51))
        self.label_19.setStyleSheet(f"border-image: url(:zdjecia/images/zgd.png);")
        self.label_19.setText("")
        self.label_19.setObjectName("label_19")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(274, 130, 111, 23))
        self.pushButton.setStyleSheet("background-color: rgb(216, 216, 216);\n"
                                      "color: rgb(0, 0, 0);")
        self.pushButton.setObjectName("pushButton")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(10, 10, 381, 91))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.label_2.setFont(font)
        self.label_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_2.setObjectName("label_2")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

        self.pushButton.clicked.connect(lambda: self.close())

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Uwaga"))
        self.pushButton.setText(_translate("Dialog", "Ok"))
        self.label_2.setText(_translate("Dialog", f"{self.mess}"))