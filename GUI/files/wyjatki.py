from werkzeug.security import generate_password_hash, check_password_hash
class Error(Exception):
   def __init__(self):
       Exception.__init__(self)

class Hash(Error):
   def __init__(self,arg,arg2):
       self.nie_zhashowane_haslo = arg
       self.zhashowane_haslo = arg2

def SprawdzCzyHasloJestPoprawne(arg,arg2):
    if check_password_hash(arg,arg2) == False:
        raise Hash(arg,arg2)
    else:
        return True

class Fraza(Error):
   def __init__(self,arg,arg2):
       self.nowa_fraza = arg
       self.powtorzona_fraza = arg2

def SprawdzCzyObieFrazySaTakieSame(arg,arg2):
    if arg != arg2:
        raise Fraza(arg,arg2)

class FrazaDnia(Error):
   def __init__(self,arg):
       self.text = arg

def SprawdzDzienTygodnia(data):
    data = int(data) #bo ta kochana data zapisuje sie jako string >:(
    x = ""
    if data % 7 == 0:
        x = "Grappa Ice"
    elif data % 7 == 1:
        x = "Infotech"
    elif data % 7 == 2:
        x = "e-slownik gurom"
    elif data % 7 == 3:
        x = "Projekty z PO juhu!!1!"
    elif data % 7 == 4:
        x = "Klasa 2A (niedługo 3)"
    elif data % 7 == 5:
        x = "Cichosz!!"
    elif data % 7 == 6:
        x = "Nauczanie zdalne R.I.P"
    elif data % 7 == 7:
        x = "Coronavirus"
    raise FrazaDnia(x)
