from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, autoincrement=True)
    login = Column(String(100))
    haslo = Column(String(100))
    email = Column(String(100))
    data = Column(String(16))


class Dictionary(Base):
    __tablename__ = 'dictionary'

    id = Column(Integer, primary_key=True, autoincrement=True)
    phrase = Column(String(100))
    definition = Column(String(10000))
    data = Column(String(16))
    author = Column(String(100))

class PhraseOnDay(Base):
    __tablename__ = 'phraseonday'

    id = Column(Integer, primary_key=True, autoincrement=True)
    phrase = Column(String(100))
    data = Column(String(16))


Session = sessionmaker()
eng = create_engine('mysql+pymysql://freedbtech_eslownik:slownikpo@freedb.tech/freedbtech_eslownik')
Session.configure(bind=eng)
session = Session()
